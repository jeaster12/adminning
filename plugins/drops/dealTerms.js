

var width = 0;
/* Float Label Pattern Plugin for Bootstrap 3.1.0 by Travis Wilson */
(function($) {
    $.fn.floatLabels = function(options) {

        // Settings
        var self = this;
        var settings = $.extend({}, options);

        // Event Handlers
        function registerEventHandlers() {
            self.on('input keyup change', 'input, textarea',  function() {
                actions.swapLabels(this);
            });

            self.on('focus', 'input, textarea',  function() {
                actions.swapLabelsOnFocus(this);
            });

            self.on('blur', 'input, textarea',  function() {
                actions.swapLabelsOnBlur(this);
            });
        }

        // Actions
        var actions = {
            initialize: function() {
                self.each(function() {
                    var $this = $(this);
                    var $label = $this.children('label');
                    var $field = $this.find('input,textarea');

                    if ($this.children().first().is('label')) {
                        $this.children().first().remove();
                        $this.append($label);
                    }

                    var placeholderText = ($field.attr('placeholder') && $field.attr('placeholder') != $label.text()) ? $field.attr('placeholder') : $label.text();

                    $label.data('placeholder-text', placeholderText);
                    $label.data('original-text', $label.text());

                    if ($field.val() === '') {
                        $field.addClass('empty');
                    }

                });
            },
            swapLabels: function (field) {
                var $field = $(field);
                var $label = $(field).siblings('label').first();
                var isEmpty = Boolean($field.val());

                if (isEmpty) {
                    $field.removeClass('empty');
                    $label.text($label.data('original-text'));
                }
                else {
                    $field.addClass('empty');
                    $label.text($label.data('placeholder-text'));
                }
                },
            swapLabelsOnFocus: function (field) {
                var $field = $(field);
                var $sel_col = $field.closest('div[class^="col-"]');

                $field.removeClass('empty');
                $sel_col.removeClass('filled');
                $sel_col.addClass('selected')
            },
            swapLabelsOnBlur: function (field) {
                var $field = $(field);
                var $sel_input = $field.closest('div[class^="col-"]');

                var isEmpty = Boolean($field.val());
                var required = false;
                var multi_select = true;

                if($sel_input.hasClass('required')) {
                    required = true;
                }

                // if previous field has a multi-tag class it is not empty
                if($sel_input.find('.multi-tag').length == 0) {
                    multi_select = false;
                }

                if($sel_input.hasClass('multiselect')){
                    if (!multi_select) {
                        $field.addClass('empty');
                        $sel_input.removeClass('selected');
                        $sel_input.removeClass('filled');
                    }else {
                        $sel_input.addClass('filled');

                        /* If the element is required it will re-bold after
                         * losing focus if no text is present */
                        if (required){
                            $sel_input.addClass('required');
                        }
                        $sel_input.removeClass('selected');
                    }
                }else {
                    if (!isEmpty) {
                        $field.addClass('empty');
                        $sel_input.removeClass('selected');
                        $sel_input.removeClass('filled');
                    }else {
                        $sel_input.addClass('filled');

                        /* If the element is required it will re-bold after
                         * losing focus if no text is present */
                        if (required){
                            $sel_input.addClass('required');
                        }
                        $sel_input.removeClass('selected');
                    }
                }
            }
        };

        // Initialization
        function init() {
            registerEventHandlers();
            actions.initialize();

            self.each(function() {
                actions.swapLabels($(this).find('input,textarea').first());
            });
        }
        init();
        return this;
    }
    ;

    $(function() {
        $('.float-label-control').floatLabels();
    })
})(jQuery);

$(document).ready(function() {

    $('input').each(function() {

        $(this).on('focus', function() {
            $(this).parent('.css').addClass('active');
        });

        $(this).on('blur', function() {
            if ($(this).val().length == 0) {
                $(this).parent('.css').removeClass('active');
            }
        });

        if ($(this).val() != '') $(this).parent('.css').addClass('active');
    })
    
});

$(function() {
    var availableTags  = [
        "value1",
        "value2",
        "value3",
        "value4",
        "value5",
        "value6"
    ];
    var multi_tags  = [
    ];
    function split( val ) {
        return val.split( /,\s*/ );
    }
    function extractLast( term ) {
        return split( term ).pop();
    }
    function build_tags ( event, ui ){


        return true;
    }
    $( "#listing" )
        // don't navigate away from the field on tab when selecting an item
        .bind( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                $( this ).autocomplete( "instance" ).menu.active ) {
                event.preventDefault();
                // call new select function
            }
        })
        .autocomplete({
            delay: 0,
            minLength: 0,
            source: function( request, response ) {
                // delegate back to autocomplete, but extract the last term
                response( $.ui.autocomplete.filter(
                    availableTags, extractLast( request.term ) ) );
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            select: function( event, ui ) {
                var selected = ui.item.value;
                // push value to multi_tags
                multi_tags.push(selected);

                // create tag element
                var term = '<span class="multi-tag" id="' + selected + '">' + selected.toString() + '<i id="remove_' + selected + '" onclick="remove_tag(this)" class="fa fa-times-circle remove-item">' + '</i></span>' + " " ;

                // If there are not tags in the field
                if( $(this).closest('div[class^="col-"]').find('.multi-tag').length > 0) {
                    // append to the span
                    $(this).closest('div[class^="col-"]').find('span').last().after(term);
                }else {
                    $( "#listing" ).parent().prepend(term);
                }

                // push tag element width to multi_tags + 9 for padding
                element_size =  $( "#" + selected.toString()).width();
                fa_size = $( "#remove_" + selected.toString()).width() + 7;

                // calculate length of all elements
                var elem_length = element_size + fa_size;

                width = $(this).closest('div[class^="col-"]').find('input').first().width();
                width = width - elem_length;
                $('#listing').width(width);

                this.value = ( "" );
                return false;
            }
        })
        .click(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        });

    $( "#seniority" )
        // don't navigate away from the field on tab when selecting an item
        .bind( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                $( this ).autocomplete( "instance" ).menu.active ) {
                event.preventDefault();
            }
        })
        .autocomplete({
            delay: 0,
            minLength: 0,
            source: function( request, response ) {
                // delegate back to autocomplete, but extract the last term
                response( $.ui.autocomplete.filter(
                    availableTags, extractLast( request.term ) ) );
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            select: function( event, ui ) {
                var selected = ui.item.value;
                // push value to multi_tags
                multi_tags.push(selected);

                // create tag element
                var term = '<span class="multi-tag" id="' + selected + '">' + selected.toString() + '<i id="remove_' + selected + '" onclick="remove_tag(this)" class="fa fa-times-circle remove-item">' + '</i></span>' + " " ;

                // If there are not tags in the field
                if( $(this).closest('div[class^="col-"]').find('.multi-tag').length > 0) {
                    // append to the span
                    $(this).closest('div[class^="col-"]').find('span').last().after(term);
                }else {
                    $( "#seniority" ).parent().prepend(term);
                }

                // push tag element width to multi_tags + 9 for padding
                var element_size =  $( "#" + selected.toString()).width();
                var fa_size = $( "#remove_" + selected.toString()).width() + 9;
                //multi_tags.push(element_size);

                // calculate length of all elements
                var elem_length = element_size + fa_size;

                width = $(this).closest('div[class^="col-"]').find('input').first().width();
                width = width - elem_length;
                $('#seniority').width(width);

                this.value = ( "" );
                return false;
            }
        })
        .click(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        });
    $( "#governing_law" )
        // don't navigate away from the field on tab when selecting an item
        .bind( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                $( this ).autocomplete( "instance" ).menu.active ) {
                event.preventDefault();
            }
        })
        .autocomplete({
            delay: 0,
            minLength: 0,
            source: function( request, response ) {
                // delegate back to autocomplete, but extract the last term
                response( $.ui.autocomplete.filter(
                    availableTags, extractLast( request.term ) ) );
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            select: function( event, ui ) {
                var selected = ui.item.value;
                // push value to multi_tags
                multi_tags.push(selected);

                // create tag element
                var term = '<span class="multi-tag" id="' + selected + '">' + selected.toString() + '<i id="remove_' + selected + '" onclick="remove_tag(this)" class="fa fa-times-circle remove-item">' + '</i></span>' + " " ;

                // If there are not tags in the field
                if( $(this).closest('div[class^="col-"]').find('.multi-tag').length > 0) {
                    // append to the span
                    $(this).closest('div[class^="col-"]').find('span').last().after(term);
                }else {
                    $( "#governing_law" ).parent().prepend(term);
                }

                // push tag element width to multi_tags + 9 for padding
                var element_size =  $( "#" + selected.toString()).width();
                var fa_size = $( "#remove_" + selected.toString()).width() + 9;
                //multi_tags.push(element_size);

                // calculate length of all elements
                var elem_length = element_size + fa_size;

                width = $(this).closest('div[class^="col-"]').find('input').first().width();
                width = width - elem_length;
                $('#governing_law').width(width);

                this.value = ( "" );
                return false;
            }
        })
        .click(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        });
    $( "#no_sale_to" )
        // don't navigate away from the field on tab when selecting an item
        .bind( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                $( this ).autocomplete( "instance" ).menu.active ) {
                event.preventDefault();
            }
        })
        .autocomplete({
            delay: 0,
            minLength: 0,
            source: function( request, response ) {
                // delegate back to autocomplete, but extract the last term
                response( $.ui.autocomplete.filter(
                    availableTags, extractLast( request.term ) ) );
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            select: function( event, ui ) {
                var selected = ui.item.value;
                // push value to multi_tags
                multi_tags.push(selected);

                // create tag element
                var term = '<span class="multi-tag" id="' + selected + '">' + selected.toString() + '<i id="remove_' + selected + '" onclick="remove_tag(this)" class="fa fa-times-circle remove-item">' + '</i></span>' + " " ;

                // If there are not tags in the field
                if( $(this).closest('div[class^="col-"]').find('.multi-tag').length > 0) {
                    // append to the span
                    $(this).closest('div[class^="col-"]').find('span').last().after(term);
                }else {
                    $( "#no_sale_to" ).parent().prepend(term);
                }

                // push tag element width to multi_tags + 9 for padding
                var element_size =  $( "#" + selected.toString()).width();
                var fa_size = $( "#remove_" + selected.toString()).width() + 9;
                //multi_tags.push(element_size);

                // calculate length of all elements
                var elem_length = element_size + fa_size;

                width = $(this).closest('div[class^="col-"]').find('input').first().width();
                width = width - elem_length;
                $('#no_sale_to').width(width);

                this.value = ( "" );
                return false;
            }
        })
        .click(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        });
    $( "#active_bookrunners" )
        // don't navigate away from the field on tab when selecting an item
        .bind( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                $( this ).autocomplete( "instance" ).menu.active ) {
                event.preventDefault();
            }
        })
        .autocomplete({
            delay: 0,
            minLength: 0,
            source: function( request, response ) {
                // delegate back to autocomplete, but extract the last term
                response( $.ui.autocomplete.filter(
                    availableTags, extractLast( request.term ) ) );
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            select: function( event, ui ) {
                var selected = ui.item.value;
                // push value to multi_tags
                multi_tags.push(selected);

                // create tag element
                var term = '<span class="multi-tag" id="' + selected + '">' + selected.toString() + '<i id="remove_' + selected + '" onclick="remove_tag(this)" class="fa fa-times-circle remove-item">' + '</i></span>' + " " ;

                // If there are not tags in the field
                if( $(this).closest('div[class^="col-"]').find('.multi-tag').length > 0) {
                    // append to the span
                    $(this).closest('div[class^="col-"]').find('span').last().after(term);
                }else {
                    $( "#active_bookrunners" ).parent().prepend(term);
                }

                // push tag element width to multi_tags + 9 for padding
                var element_size =  $( "#" + selected.toString()).width();
                var fa_size = $( "#remove_" + selected.toString()).width() + 9;
                //multi_tags.push(element_size);

                // calculate length of all elements
                var elem_length = element_size + fa_size;

                width = $(this).closest('div[class^="col-"]').find('input').first().width();
                width = width - elem_length;
                $('#active_bookrunners').width(width);

                this.value = ( "" );
                return false;
            }
        })
        .click(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        });
    $( "#passive_bookrunners" )
        // don't navigate away from the field on tab when selecting an item
        .bind( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                $( this ).autocomplete( "instance" ).menu.active ) {
                event.preventDefault();
            }
        })
        .autocomplete({
            delay: 0,
            minLength: 0,
            source: function( request, response ) {
                // delegate back to autocomplete, but extract the last term
                response( $.ui.autocomplete.filter(
                    availableTags, extractLast( request.term ) ) );
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            select: function( event, ui ) {
                var selected = ui.item.value;
                // push value to multi_tags
                multi_tags.push(selected);

                // create tag element
                var term = '<span class="multi-tag" id="' + selected + '">' + selected.toString() + '<i id="remove_' + selected + '" onclick="remove_tag(this)" class="fa fa-times-circle remove-item">' + '</i></span>' + " " ;

                // If there are not tags in the field
                if( $(this).closest('div[class^="col-"]').find('.multi-tag').length > 0) {
                    // append to the span
                    $(this).closest('div[class^="col-"]').find('span').last().after(term);
                }else {
                    $( "#passive_bookrunners" ).parent().prepend(term);
                }

                // push tag element width to multi_tags + 9 for padding
                var element_size =  $( "#" + selected.toString()).width();
                var fa_size = $( "#remove_" + selected.toString()).width() + 9;
                //multi_tags.push(element_size);

                // calculate length of all elements
                var elem_length = element_size + fa_size;

                width = $(this).closest('div[class^="col-"]').find('input').first().width();
                width = width - elem_length;
                $('#passive_bookrunners').width(width);

                this.value = ( "" );
                return false;
            }
        })
        .click(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        });
    $( "#comanagers" )
        // don't navigate away from the field on tab when selecting an item
        .bind( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                $( this ).autocomplete( "instance" ).menu.active ) {
                event.preventDefault();
            }
        })
        .autocomplete({
            delay: 0,
            minLength: 0,
            source: function( request, response ) {
                // delegate back to autocomplete, but extract the last term
                response( $.ui.autocomplete.filter(
                    availableTags, extractLast( request.term ) ) );
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            select: function( event, ui ) {
                var selected = ui.item.value;
                // push value to multi_tags
                multi_tags.push(selected);

                // create tag element
                var term = '<span class="multi-tag" id="' + selected + '">' + selected.toString() + '<i id="remove_' + selected + '" onclick="remove_tag(this)" class="fa fa-times-circle remove-item">' + '</i></span>' + " " ;

                // If there are not tags in the field
                if( $(this).closest('div[class^="col-"]').find('.multi-tag').length > 0) {
                    // append to the span
                    $(this).closest('div[class^="col-"]').find('span').last().after(term);
                }else {
                    $( "#comanagers" ).parent().prepend(term);
                }

                // push tag element width to multi_tags + 9 for padding
                var element_size =  $( "#" + selected.toString()).width();
                var fa_size = $( "#remove_" + selected.toString()).width() + 9;
                //multi_tags.push(element_size);

                // calculate length of all elements
                var elem_length = element_size + fa_size;

                width = $(this).closest('div[class^="col-"]').find('input').first().width();
                width = width - elem_length;
                $('#comanagers').width(width);

                this.value = ( "" );
                return false;
            }
        })
        .click(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        });
});

/* dropdown autocomplete jquery ui */
$(function() {
    var tags = [
        "value1",
        "value2",
        "value3",
        "value4",
        "value5",
        "value6"
    ];
    $( "#moodys_rating" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;
            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
    });
    $( "#issuer" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;
            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#sp_rating" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;
            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#fitch_rating" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;
            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#g_moody_rating" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;
            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#g_sp_rating" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;
            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#g_fitch_rating" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;
            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#tr_sp_rating" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;
            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#tr_moody_rating" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;
            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#tr_fitch_rating" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;
            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#currency" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;
            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#tr_currency" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;
            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#country_of_risk" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;
            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#country_of_issue" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;
            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#issuer_type" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;
/*
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
            $sel_col.addClass('filled');
*/
            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#bond_rating_type" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;

            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
            $sel_col.addClass('filled');

            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#security_type" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;

            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
            $sel_col.addClass('filled');

            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#transaction_type" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;

            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
            $sel_col.addClass('filled');

            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#sales_restrictions" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;

            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
            $sel_col.addClass('filled');

            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#form" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;

            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
            $sel_col.addClass('filled');

            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#erisa_eligible" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;

            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
            $sel_col.addClass('filled');

            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#debt_programme" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;

            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
            $sel_col.addClass('filled');

            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#coupon_day_count" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;

            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
            $sel_col.addClass('filled');

            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#coupon_frequency" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;

            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
            $sel_col.addClass('filled');

            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#first_coupon_date" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;

            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
            $sel_col.addClass('filled');

            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#long_short_coupon" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;

            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
            $sel_col.addClass('filled');

            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#pricing_references" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;

            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
            $sel_col.addClass('filled');

            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#benchmark_bond" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;

            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
            $sel_col.addClass('filled');

            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#trade_date" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;

            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
            $sel_col.addClass('filled');

            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#settlement_date" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;

            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
            $sel_col.addClass('filled');

            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#interest_accrual_date" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;

            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
            $sel_col.addClass('filled');

            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#bd_bank" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;

            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
            $sel_col.addClass('filled');

            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#docs_bank" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;

            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
            $sel_col.addClass('filled');

            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#hedge_manager" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;

            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
            $sel_col.addClass('filled');

            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
    $( "#maturity_date" ).autocomplete({
        minLength: 0,
        delay: 0,
        source: tags,
        select: function( event, ui ) {
            var selected = ui.item.value;
            this.value = selected;

            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
            $sel_col.addClass('filled');

            return false;
        }
    })
        .focus(function(){
            $(this).autocomplete('search', $(this).val())
        })
        .blur(function() {
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('selected');
        })
        .click(function(){
            var $sel_col = $(this).closest('div[class^="col-"]');
            $sel_col.removeClass('filled');
            $sel_col.addClass('selected');
            $(this).autocomplete('search', "")
        });
});

function remove_tag(elem) {

    // column and input
    var $sel_col =  $(elem).closest('div[class^="col-"]');
    var $sel_input =  $sel_col.find('input').first();

    // get width of input
    width = $sel_input.width();

    // temp sizes of tag and fa element
    var tag_width =  $(elem).width() + 7 + $(elem).parent('span').width();
    var new_width = width + tag_width;

    $sel_input.width(new_width);

    $(elem).parent('span').remove();

    // gets array of multi-tags length = 0 there are no multi-tags
    var num_multi_tags = $sel_col.find('.multi-tag').length;

    // if there is not .multi-tag remove filled state
    if(num_multi_tags == 0) {
        $sel_input.addClass('empty');
        $sel_col.removeClass('filled');
    }
}

function nav_save_window(){
    $('#save_window').show();
}

function nav_messages_window(){
    $('#message_window').show();
}

function nav_publish_window(){
    $('#publish_window').show();
}

function nav_add_window(){
    $('#add_window').show();
}

function nav_history_window(){
    $('#history_window').show();
}

function nav_registration_window(){
    $('#registrations_window').show();
}

function nav_delete_tranche_window(){
    $('#delete_tranche_window').show();
}

function nav_delete_deal_window(){
    $('#delete_deal_window').show();
}

function nav_help_window(){
    $('#help_window').show();
}

function remove_nav_window(elem){
    $(elem).closest('.container-window').hide();
}

$(function() {
    $( "#save_window" )
        .draggable({
            handle: '.nav-menu-draggable-top-bar'
        })
        .resizable({
            handles: 'n, e, s, w, ne, se, sw, nw'
        })
    $( "#message_window" )
        .draggable({
            handle: '.nav-menu-draggable-top-bar'
        })
        .resizable({
            handles: 'n, e, s, w, ne, se, sw, nw'
        })
    $( "#publish_window" )
        .draggable({
            handle: '.nav-menu-draggable-top-bar'
        })
        .resizable({
            handles: 'n, e, s, w, ne, se, sw, nw'
        })
    $( "#add_window" )
        .draggable({
            handle: '.nav-menu-draggable-top-bar'
        })
        .resizable({
            handles: 'n, e, s, w, ne, se, sw, nw'
        })
    $( "#history_window" )
        .draggable({
            handle: '.nav-menu-draggable-top-bar'
        })
        .resizable({
            handles: 'n, e, s, w, ne, se, sw, nw'
        })
    $( "#registrations_window" )
        .draggable({
            handle: '.nav-menu-draggable-top-bar'
        })
        .resizable({
            handles: 'n, e, s, w, ne, se, sw, nw'
        })
    $( "#delete_tranche_window" )
        .draggable({
            handle: '.nav-menu-draggable-top-bar'
        })
        .resizable({
            handles: 'n, e, s, w, ne, se, sw, nw'
        })
    $( "#delete_deal_window" )
        .draggable({
            handle: '.nav-menu-draggable-top-bar'
        })
        .resizable({
            handles: 'n, e, s, w, ne, se, sw, nw'
        })
    $( "#help_window" )
        .draggable({
            handle: '.nav-menu-draggable-top-bar'
        })
        .resizable({
            handles: 'n, e, s, w, ne, se, sw, nw'
        })
});

// adds up to 2 more tranches
function add_tranche(elem){
    var $container2 = $('#ct2');
    var $container3 = $('#ct3');

    if($container2.is(":hidden")){
        $container2.show();
    }else if($container3.is(":hidden")){
        $container3.show();
    }
}

function link_unlink (elem) {
    var $container1 = $('#ct1');
    var $container2 = $('#ct2');

    // the element is in a linked state -> move to unlinked state
    if($(elem).hasClass('fa-link')){
        // change linked to unlinked
        $(elem).addClass('fa-chain-broken').removeClass('fa-link');

        // if container 2 is not hidden
        if(!($container2.is(":hidden"))){
            var input_id = "#" + $(elem).next().get(0).id.slice(0,-1);
            var $sel_col = $($container1).find(input_id).closest('div[class^="col-"]');

            // clear text values
            $(elem).next().val("");
            $(elem).next().addClass('empty');
            $(elem).next().removeClass('filled');

            // enable text field
            $(elem).next().prop('disabled', false);
            // turn field gray
            $(elem).closest('div[class^="col-"]').removeClass('form-grid-disabled');
            // change unlinked icon to linked
            $($sel_col).find('.fa-link').addClass('fa-chain-broken').removeClass('fa-link');
        }

    }else{ // element is in a unlinked state -> move to linked state
        // changed unlinked to linked
        $(elem).addClass('fa-link').removeClass('fa-chain-broken');

        // if container 2 is not hidden
        if( !($container2.is(":hidden")) ){
            var input_id = "#" + $(elem).next().get(0).id.slice(0,-1);
            var $sel_col = $($($container1).find(input_id)).closest('div[class^="col-"]');

            //copy text from input field
            var selected =  $($container1.find($(input_id))).val();
            $(elem).next().val(selected);
            $(elem).next().removeClass('empty');
            $(elem).next().addClass('filled');

            // disable text field
            $(elem).next().prop('disabled', true);
            // turn field gray
            $(elem).closest('div[class^="col-"]').addClass('form-grid-disabled');
            // change unlinked icon to linked
            $($sel_col).find('.fa-chain-broken').addClass('fa-link').removeClass('fa-chain-broken');

        }
    }
}

function enable_linking(elem) {
    var $container2 = $('#ct2');

    // if container 2 exist modify if not do nothing
    if( !($container2.is(":hidden")) ) {
        // If the linking button is enabled revert back to normal state
        if ($(elem).hasClass('linking-button-enabled')) {
            $(elem).removeClass('linking-button-enabled');
            $('.fa-chain-broken').css({"display": "none"});
            $('.fa-link').css({"display": "none"});
            $(elem).addClass('linking-button');
            $($($container2).find('.fa-link')).click();
        } else { // change linking buttons state and enable linking
            $(elem).addClass('linking-button-enabled');
            $('.fa-chain-broken').css({"display": "inline-block"});
            $('.fa-link').css({"display": "inline-block"});
            $($($container2).find('.fa-chain-broken')).click();
        }
    }
}



function reload() {
    location.reload();
}




$("#checkBox").click(function () {
      if ($("#check").is(':checked'))
      {
         $(".checkBoxWrapper").css("background" , "#f2f2f2" );
      }
      else 
      {
         $(".checkBoxWrapper").css("background" , "#2dcc70" );
      }
});