/*=========================================
 * author: João Pereira
 * website: http://www.joaopereira.pt
 * email: joaopereirawd@gmail.com
=========================================*/

/*=========================================
 * animatedModal's scripts
=========================================*/
$(document).ready(function() {

    //demo 01
    $("#demo01").animatedModal({
        animatedIn:'fade-in-up-lg',
        animatedOut:'fade-out-up-sm',
        color:'#ffffff',
        beforeOpen: function() {

            var children = $(".thumb");
            var index = 0;

            function addClassNextChild() {
                if (index == children.length) return;
                children.eq(index++).show().velocity("transition.expandIn", { opacity:1, stagger: 250 });
                window.setTimeout(addClassNextChild, 200);
            }

            addClassNextChild();

        },
        afterClose: function() {
          $(".thumb").hide();
              
        }
    });



}); // end document ready


/*========================================
 * Webkit detection because animations SVG 
   in different browsers
=========================================*/
var isWebkit = 'WebkitAppearance' in document.documentElement.style;

if (isWebkit) {
 webkitAnimation();
} else {
  nonwebkit();
}


/*========================================
 * Animation for webkit with velocity.js
=========================================*/
function webkitAnimation() {
  $("#browser-container")
        .velocity(
          { 
            top: -40
          }, 900, 
          "easeInOutExpo"
        )
        .velocity(
          { 
            top: -20 
          }, 700,
           "easeOutExpo"
        );


  $("#btn-openModal")
      .velocity(
          { 
            opacity:1
          }, 
          { 
            delay: 800
          }
      );


  $("#cursor")
    .delay(900) // animation delay 
    // cursor movement to 'load'
    .velocity(
      { 
        translateX: 0, 
        translateY: -40 
      }
    )
    .delay(200) // makes delay
    //makes cursor movement back
    .velocity(
      {
        translateY: -5,
        scaleY:0.9
      },
      {
        duration: 100,
            // get the animation
            begin: function() {  
               $("#btn-openModal")
                    // Button animation when you press
                    .velocity(
                      { 
                        scale:0.9 
                      },110
                    )
                    .velocity(
                      "reverse", 
                      {
                       duration: 110 
                      }
                    );
            }
      }
    )
    .velocity(
          { 
            translateY: -40,
            scaleY:1
          },
          {
            duration: 100,
            // when movements of the cursor and button are concluded - triggers the animation of the modal
            complete: function() {
                $("#modal")
                    .velocity(
                        { 
                          scale:0
                        }
                    ) 
                    .velocity(
                        { 
                          opacity:1,
                          scale:1
                        },
                        // When the animation is modal in early - triggers the animation button close modal
                        {
                           duration:200,
                          begin: function() {
                              $("#modal-btn-close")
                                  .delay(100)
                                  .velocity("transition.expandIn", { opacity:1, stagger: 250 });
                              $("#el-01")
                                  .delay(200)
                                  .velocity("transition.expandIn", { opacity:1, stagger: 250 });
                              $("#el-02")
                                  .delay(250)
                                  .velocity("transition.expandIn", { opacity:1, stagger: 250 });

                              $("#el-03")
                                  .delay(350)
                                  .velocity("transition.expandIn", { opacity:1, stagger: 250 });

                          }
                        }
                    );        
            }

          }
      );

}
   
function nonwebkit() {


  $("#browser-container")
        .velocity(
          { 
            top: -40
          }, 900, 
          "easeInOutExpo"
        )
        .velocity(
          { 
            top: -20 
          }, 700,
           "easeOutExpo"
        );


  $("#btn-openModal")
      .velocity(
          { 
            opacity:1
          }, 
          { 
            delay: 800
          }
      );


  $("#cursor")
    .delay(900) // animation delay 
    // movimento do cursor a 'carregar'
    .velocity(
      { 
        translateX: 0, 
        translateY: -40 
      }
    )
    .delay(200) // faz um delay
    //faz o movimento do cursor para trás 
    .velocity(
      {
        translateY: -5,
        scaleY:0.9
      },
      {
        duration: 100,
            // mal começa a animação faz a animação de botão pressionado.
            begin: function() {  
               $("#btn-openModal")
                    // animação do botão quando é pressionado 
                    .velocity(
                      { 
                        scale:0.9 
                      },110
                    )
                    .velocity(
                      "reverse", 
                      {
                       duration: 110 
                      }
                    );
            }
      }
    )
    .velocity(
          { 
            translateY: -40,
            scaleY:1
          },
          {
            duration: 100,
            // quando os movimentos do cursor e do botao estão concluidos dispara a animação da modal
            complete: function() {
                $("#modal")
                    .velocity(
                        { 
                          scale:0
                        }
                    ) 
                    .velocity(
                        { 
                          opacity:1,
                          scale:1
                        },
                        // Quando a animação da modal está no inicio dispara a animação do botão fechar modal
                        {
                           duration:200,
                          begin: function() {
                              $("#modal-btn-close")
                                  .delay(100)
                                  .velocity("transition.slideLeftIn", { opacity:1, stagger: 250 });
                              $("#el-01")
                                  .delay(200)
                                  .velocity("transition.slideLeftIn", { opacity:1, stagger: 250 });
                              $("#el-02")
                                  .delay(250)
                                  .velocity("transition.slideLeftIn", { opacity:1, stagger: 250 });

                              $("#el-03")
                                  .delay(350)
                                  .velocity("transition.slideLeftIn", { opacity:1, stagger: 250 });

                          }
                        }
                    );        
            }

          }
      );

}

/*=========================================
 * WOW.JS
=========================================*/
$(document).ready(function() {
        var wow = new WOW(
          {
            boxClass:     'wow',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset:       0,          // distance to the element when triggering the animation (default is 0)
            mobile:       false        // trigger animations on mobile devices (true is default)
          }
        );
        wow.init();
});




/*=========================================
 * Tooltips
=========================================*/
$(function() {
	
	
	$('#upload-docs').tooltipster({
		content: $('<div style="float:left; margin:12px 12px 8px 12px;"><p style="text-align:left; color:#D44F53; margin-top:2px;"><strong>You must add a Document Type and a Document Name to include when distributing.</p></strong></div>'),
		// setting a same value to minWidth and maxWidth will result in a fixed width
		minWidth: 300,
		maxWidth: 300,
        theme: 'tooltipster-shadow',
        animation: 'grow',
		position: 'right'
	});

    	$('#close-docs').tooltipster({
		content: $('<div style="float:left; margin:12px 12px 8px 12px;"><p style="text-align:left; color:#D44F53; margin-top:2px;"><strong>You must complete all required fields to collapse the Documents window.</p></strong></div>'),
		// setting a same value to minWidth and maxWidth will result in a fixed width
		minWidth: 300,
		maxWidth: 300,
        theme: 'tooltipster-shadow',
        animation: 'grow',
		position: 'right'
	});
    

	$('.tooltipster-shadow-preview').tooltipster({
		theme: 'tooltipster-shadow'
	});
	

	
});


document.addEventListener('DOMContentLoaded', function() {
	[].forEach.call(document.querySelectorAll('.add'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('docUpload').classList.remove('hide');
		});
	});
	[].forEach.call(document.querySelectorAll('.add'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('docRow').classList.add('hide');
		});
	});
	[].forEach.call(document.querySelectorAll('.add'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('docUpload').classList.add('animated');
		});
	});
	[].forEach.call(document.querySelectorAll('.add'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('docUpload').classList.add('fadeIn');
		});
	});
	document.getElementById('uploadBtn').addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('uploading').classList.remove('hide');
	});
	document.getElementById('uploadBtn').addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('uploading').classList.add('animated');
	});
	document.getElementById('uploadBtn').addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('uploading').classList.add('fadeIn');
	});
	[].forEach.call(document.querySelectorAll('.row-right'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('allInfo').classList.add('hide');
		});
	});
	[].forEach.call(document.querySelectorAll('.row-right'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('deleting').classList.remove('hide');
		});
	});
	document.getElementById('delete').addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('uploading').classList.add('hide');
	});
	document.getElementById('delete').addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('allInfo').classList.remove('hide');
	});
	document.getElementById('cancel').addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('allInfo').classList.remove('hide');
	});
	document.getElementById('cancel').addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('deleting').classList.add('hide');
	});
});



var width = 0;
/* Float Label Pattern Plugin for Bootstrap 3.1.0 by Travis Wilson */
(function($) {
    $.fn.floatLabels = function(options) {

        // Settings
        var self = this;
        var settings = $.extend({}, options);

        // Event Handlers
        function registerEventHandlers() {
            self.on('input keyup change', 'input, textarea',  function() {
                actions.swapLabels(this);
            });

            self.on('focus', 'input, textarea',  function() {
                actions.swapLabelsOnFocus(this);
            });

            self.on('blur', 'input, textarea',  function() {
                actions.swapLabelsOnBlur(this);
            });
        }

        // Actions
        var actions = {
            initialize: function() {
                self.each(function() {
                    var $this = $(this);
                    var $label = $this.children('label');
                    var $field = $this.find('input,textarea');

                    if ($this.children().first().is('label')) {
                        $this.children().first().remove();
                        $this.append($label);
                    }

                    var placeholderText = ($field.attr('placeholder') && $field.attr('placeholder') != $label.text()) ? $field.attr('placeholder') : $label.text();

                    $label.data('placeholder-text', placeholderText);
                    $label.data('original-text', $label.text());

                    if ($field.val() === '') {
                        $field.addClass('empty');
                    }

                });
            },
            swapLabels: function (field) {
                var $field = $(field);
                var $label = $(field).siblings('label').first();
                var isEmpty = Boolean($field.val());

                if (isEmpty) {
                    $field.removeClass('empty');
                    $label.text($label.data('original-text'));
                }
                else {
                    $field.addClass('empty');
                    $label.text($label.data('placeholder-text'));
                }
                },
            swapLabelsOnFocus: function (field) {
                var $field = $(field);
                var $sel_col = $field.closest('div[class^="col-"]');

                $field.removeClass('empty');
                $sel_col.removeClass('filled');
                $sel_col.addClass('selected')
            },
            swapLabelsOnBlur: function (field) {
                var $field = $(field);
                var $sel_input = $field.closest('div[class^="col-"]');

                var isEmpty = Boolean($field.val());
                var required = false;
                var multi_select = true;

                if($sel_input.hasClass('required')) {
                    required = true;
                }

                // if previous field has a multi-tag class it is not empty
                if($sel_input.find('.multi-tag').length == 0) {
                    multi_select = false;
                }

                if($sel_input.hasClass('multiselect')){
                    if (!multi_select) {
                        $field.addClass('empty');
                        $sel_input.removeClass('selected');
                        $sel_input.removeClass('filled');
                    }else {
                        $sel_input.addClass('filled');

                        /* If the element is required it will re-bold after
                         * losing focus if no text is present */
                        if (required){
                            $sel_input.addClass('required');
                        }
                        $sel_input.removeClass('selected');
                    }
                }else {
                    if (!isEmpty) {
                        $field.addClass('empty');
                        $sel_input.removeClass('selected');
                        $sel_input.removeClass('filled');
                    }else {
                        $sel_input.addClass('filled');

                        /* If the element is required it will re-bold after
                         * losing focus if no text is present */
                        if (required){
                            $sel_input.addClass('required');
                        }
                        $sel_input.removeClass('selected');
                    }
                }
            }
        };

        // Initialization
        function init() {
            registerEventHandlers();
            actions.initialize();

            self.each(function() {
                actions.swapLabels($(this).find('input,textarea').first());
            });
        }
        init();
        return this;
    }
    ;

    $(function() {
        $('.float-label-control').floatLabels();
    })
})(jQuery);

$(document).ready(function() {

    $('input').each(function() {

        $(this).on('focus', function() {
            $(this).parent('.css').addClass('active');
        });

        $(this).on('blur', function() {
            if ($(this).val().length == 0) {
                $(this).parent('.css').removeClass('active');
            }
        });

        if ($(this).val() != '') $(this).parent('.css').addClass('active');
    })
    
});



function remove_tag(elem) {

    // column and input
    var $sel_col =  $(elem).closest('div[class^="col-"]');
    var $sel_input =  $sel_col.find('input').first();

    // get width of input
    width = $sel_input.width();

    // temp sizes of tag and fa element
    var tag_width =  $(elem).width() + 7 + $(elem).parent('span').width();
    var new_width = width + tag_width;

    $sel_input.width(new_width);

    $(elem).parent('span').remove();

    // gets array of multi-tags length = 0 there are no multi-tags
    var num_multi_tags = $sel_col.find('.multi-tag').length;

    // if there is not .multi-tag remove filled state
    if(num_multi_tags == 0) {
        $sel_input.addClass('empty');
        $sel_col.removeClass('filled');
    }
}





